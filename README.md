![lock](res/img/honeypot.png)

# Mise en situation

Vous êtes en charge de la sécurité informatique dans une entreprise stratégique Suisse.
A des fins d'évaluation des menaces informatique et de veille informatique, vous décidez la mise en place d'un pot de miel ("honeypot").

# Cahier des charges

Vous décidez du développement en interne d'un outil d'analyse des traces du serveur web (solution nginx, format de log par défaut).

## chargement dynamique d'un fichier de log

Un menu dans le logiciel doit permettre à l'utilisateur de téléverser ("uploader") le fichier de log à analyser.

L'outil doit alors récupérer les informations suivantes du fichier de log:

- adresse IP source,
- date d'accès,
- URL de la ressource demandée(ex: /index.html),
- code de retour HTTP du serveur web (ex: 200, 404, 403, etc),
- type d'agent sur le système distant (ex: chrome, firefox, safari, etc).

A titre d'illustration, un exemple issu d'un fichier de log nginx est fourni ci-dessous:

```
1.3.5.7 - - [1/Jan/2000:01:02:03 +0100] "GET / HTTP/1.0" 200 41 "-" "Chrome/61.0.3163.91 Safari/537.36""
7.5.3.1 - - [2/Jan/2000:02:03:04 +0100] "GET / HTTP/1.0" 200 53 "-" "Mozilla/5.0 (Windows NT 6.1) "
```

Si le fichier n'est pas conforme, une message d'erreur doit être affiché à l'utilisateur
(ex: une image est téléversée à la place du fichier de log).

## Cartographie des attaques

Le logiciel doit ensuite résoudre chaque adresse IP en position géographique (latitude, longitude) via un service d'API externe.
Puis pour chaque attaque, un marqueur doit être affiché sur la carte.

Afin de cibler l'analyse, vous décidez d'appliquer des codes couleurs différents pour les IP effectuant souvent des attaques.

| Fréquence d'attaque  | Couleur du marqueur |
|----------------------|---------------------|
| [1,5]                | vert                |
| [6,10]               | orange              |
| >10                  | rouge               |

L'image ci-dessous donne un exemple de résultat attendu pour la carte affichant les menaces.

En cliquant sur un marqueur, une fenêtre contextuelle doit afficher les informations associée à l'attaque.
Si un marqueur sur lequel on clique a donné lieu à de nombreuses attaques, on affichera alors le nombre d'attaques ainsi que seulement la dernière attaque.

Dans le menu contextuel, la pays et la ville d'où provient cette attaque doivent aussi apparaitre.

![lock](res/img/map.jpg)

## Technologies à mettre en oeuvre

Vous avez défini les technologies suivantes pour la réalisation du logiciel:

Langage de programmation: PHP ou C#.

Librairie de cartographie: leaflet, mapbox ou google map ou openlayers.

Base de données: mysql / mariadb. A noter que l'usage d'une base de données n'est pas obligatoire.